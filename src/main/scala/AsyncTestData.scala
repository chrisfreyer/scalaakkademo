import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Random
class AsyncTestData {
}

object AsyncTestData {

  val DELAY_TIME = 200 //milliseconds


  val rnd = Random

  /**
    * randomDelay method, to make async methods in this object look more real
     */
  def randomDelay(): Unit = {
    val ms:Long = rnd.nextInt(DELAY_TIME)
    Thread.sleep(ms)
  }

  def getFuturePerson(id:Int): Future[Person] = {
    randomDelay
    if(id %2 == 1)
        return Future.successful(TestData.test_person)
    Future.successful(TestData.test_person_2)
  }

  def getFutureAddress(id:Int): Future[Address] = {
    randomDelay
    if(id %2 == 1)
      return Future.successful(TestData.test_address_1)
    Future.successful(TestData.test_address_2)
  }

  def getTupleFuturePersonFutureAddress:(Future[Person],Future[Address]) = {
    randomDelay
    return (getFuturePerson(1), getFutureAddress(1))
  }

  def getFutureListOfPersons:Future[Seq[Person]] = {
    randomDelay
    //convert a List-of-Futures to a Future List using .sequence
    Future.sequence(getListOfFuturePersons)
  }
  def getListOfFuturePersons:Seq[Future[Person]] = {
    randomDelay
    Seq(getFuturePerson(1), getFuturePerson(2))
  }

  def getFutureListOfAddresses:Future[Seq[Address]] = {
    randomDelay
    //convert a List-of-Futures to a Future List using .sequence
    Future.sequence(getListOfFutureAddresses)
  }

  def getListOfFutureAddresses:Seq[Future[Address]] = {
    randomDelay
    Seq(getFutureAddress(1), getFutureAddress(2))
  }

  def getFutureNationalId(id:Int): Future[NationalID] = {
    randomDelay
    if (id % 2 == 1)
      return Future.successful(TestData.national_id_1)
    Future.successful(TestData.national_id_2)
  }

}