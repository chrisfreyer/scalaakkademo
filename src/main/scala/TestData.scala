class TestData {}

/**
  * Define test data in the object, so its public static.
  */
object TestData {
  lazy val test_person = Person(
    id = 1,
    firstName = "Joe",
    lastName = "Smith",
    address = Some(Seq(test_address_1)),
    nationalId = Some(national_id_1),
    age = Some(33)
  )

  lazy val test_person_2 = Person(
    id = 2,
    firstName = "Marie",
    lastName = "Jones",
    address = Some(Seq(test_address_1, test_address_2)),
    nationalId = Some(national_id_2),
    age = Some(33)
  )


  val test_address_1 = Address("Home","Street1", "City1", "State1", "Zip1", "USA")
  val test_address_2 = Address("Work","Street2", "City2", "State2", "Zip2", "USA")

  val national_id_1 = NationalID(nationalIdType = "SSN", nationalIdValue = "1234567890")
  val national_id_2 = NationalID(nationalIdType = "SSN", nationalIdValue = "1234567891")

  val listOfInts:Seq[Int] = Seq(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
  val listOfDoubles:Seq[Double] = Seq(1.5, 2.0, 3.5, 4.0, 5.5, 6.0, 7.5, 8.0, 9.5)

  val personIds = PersonIds(Seq(1,2,3))
}

