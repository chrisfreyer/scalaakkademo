import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.ContentTypes.`application/json`
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.ExceptionHandler
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory
import spray.json._

import scala.io.StdIn
import scala.util.{Failure, Success}

class AppServer {

  implicit val system = ActorSystem("my-system")
  implicit val materializer = ActorMaterializer()
  // needed for the flatMap/onComplete at the end
  implicit val executionContext = system.dispatcher

  //assumes config is in src/main/resources/application.conf
  val config = ConfigFactory.load()
  val host = config.getString("akka.http.host")
  val port = config.getInt("akka.http.port")

  /**
    * Exception handler for the route.  Pretty generic
    */
  val myExceptionHandler = ExceptionHandler {
    case _: Exception =>
      extractUri { uri =>
        println(s"Request to $uri could not be handled normally")
        complete(HttpResponse(StatusCodes.InternalServerError, entity = s"Something went wrong processing $uri!"))
      }
  }

  //route handling
  val route = handleExceptions(myExceptionHandler) {
    AppServer.HelloRoute ~
      AppServer.PersonRoute ~
      AppServer.NationalIdRoute ~
      AppServer.AddressRoute ~
      AppServer.DefaultRoute
  }

  val bindingFuture = Http().bindAndHandle(route, host, port)
  println(s"Server running at http://$host:$port/\nPress ENTER to terminate...")

  def stop = {
    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ => system.terminate()) // and shutdown when done
  }
}


object AppServer extends App{
  /**
    * main is used in testing, with SBT where a console is present.
    * Run this method (via sbt 'run') and the app server will start.
    * Press enter at the sbt prompt to kill it.
    * @param args - required by API but not used
    */
  override def main(args: Array[String]): Unit = {
    val server = new AppServer
    StdIn.readLine() // let it run until user presses return
    server.stop
  }



  /**
    * basic hello world route
    * @return html string and status code 200
    */
  def HelloRoute = pathPrefix("hello") {
    complete(HttpEntity(
      contentType = ContentTypes.`text/html(UTF-8)`,
      string = "<h1>Say hello to akka-http</h1>")
    )
  }

  /**
    * rest-like interface for person
    * @return RouteResult - data & http status code
    */
  def PersonRoute = pathPrefix("person") {
    pathPrefix( IntNumber) { n =>
      onComplete(AsyncTestData.getFuturePerson(n % 2)){
        case Success(s) => complete(HttpEntity(contentType = `application/json`, string = s.toJson.toString))
        case Failure(f) => complete(HttpResponse(StatusCodes.InternalServerError))
      }

    } ~
      pathPrefix ("list"){
        onComplete(AsyncTestData.getFutureListOfPersons){
          case Success(seqPersons) => {
            //make the result look like   {"ids": [1,2]}
            val pids:PersonIds = PersonIds(seqPersons.map(_.id))
            complete(HttpEntity(contentType = `application/json`, string = pids.toJson.toString))
          }
          case Failure(f) => complete(HttpResponse(StatusCodes.InternalServerError))
        }
    } ~
      {
        (get | put) { ctx =>
        ctx.complete(HttpEntity(contentType = `application/json`, string = "{\"would_be_id\":" + 99 + "}"))
      }
    }
  }


  def NationalIdRoute = pathPrefix("nationalid") {
    pathPrefix( IntNumber) { n =>
      onComplete(AsyncTestData.getFutureNationalId(n)){
        case Success(s) => complete(HttpEntity(contentType = `application/json`, string = s.toJson.toString))
        case Failure(f) => complete(HttpResponse(StatusCodes.InternalServerError))
      }

    } ~
      pathPrefix ("list"){
        val result = "{\"ids\": [1,2]}"
        complete(HttpEntity(contentType = `application/json`, string = result))
      } ~
      {
        (get | put) { ctx =>
          ctx.complete(HttpEntity(contentType = `application/json`, string = "{\"would_be_id\":" + 3 + "}"))
        }
      }
  }

  def AddressRoute = pathPrefix("address") {
    pathPrefix( IntNumber) { n =>
      onComplete(AsyncTestData.getFutureAddress(n)){
        case Success(s) => complete(HttpEntity(contentType = `application/json`, string = s.toJson.toString))
        case Failure(f) => complete(HttpResponse(StatusCodes.InternalServerError))
      }
    } ~
      pathPrefix ("list"){
        val result = "{\"ids\": [1,2]}"
        complete(HttpEntity(contentType = `application/json`, string = result))
      } ~
      {
        (get | put) { ctx =>
          ctx.complete(HttpEntity(contentType = `application/json`, string = "{\"would_be_id\":" + 3 + "}"))
        }
      }
  }

  def DefaultRoute = get {
    complete(HttpEntity(contentType = ContentTypes.`text/html(UTF-8)`, string = "empty route"))
  }


}
