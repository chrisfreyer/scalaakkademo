import spray.json._
import DefaultJsonProtocol._

case class NationalID (
                    nationalIdType: String,
                    nationalIdValue: String
)
object NationalID extends DefaultJsonProtocol {
  implicit val nationalIdFormat = jsonFormat2(NationalID.apply)
}

case class Address(
                    addressType: String,
                    street: String,
                    city: String,
                    state: String,
                    postalCode: String,
                    country: String
)
object Address extends DefaultJsonProtocol {
  implicit val addressFormat = jsonFormat6(Address.apply)
}

case class Person (
                    id: Int,
                    firstName: String,
                    lastName: String,
                    nationalId: Option[NationalID],
                    address:  Option[Seq[Address]],
                    age: Option[Int]
                  )

object Person extends DefaultJsonProtocol {
  implicit val personFormat = jsonFormat6(Person.apply)
}


case class PersonIds (
                      ids: Seq[Int]
                     )

object PersonIds extends DefaultJsonProtocol {
  implicit val personIdsFormat = jsonFormat1(PersonIds.apply)
}