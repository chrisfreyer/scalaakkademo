import org.scalatest.{FunSpec, Matchers}

import org.scalatest.Matchers
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import spray.json._


class AppServerSpec extends FunSpec with Matchers with ScalatestRouteTest{

  describe ("Routes") {
    describe ("Person") {
      it("GET a person by ID") {
        Get("/person/1") ~> AppServer.PersonRoute ~> check {
          status shouldEqual StatusCodes.OK
          responseAs[String] shouldEqual TestData.test_person.toJson.toString
        }
      }
      it("PUT a new person") {
        Put("/person") ~> AppServer.PersonRoute ~> check {
          status shouldEqual StatusCodes.OK
          responseAs[String] shouldEqual "{\"would_be_id\":99}"
        }
      }
      it("list persons") {
        Get("/person/list") ~> AppServer.PersonRoute ~> check {
          status shouldEqual StatusCodes.OK
          responseAs[String] shouldEqual "{\"ids\":[1,2]}"
        }
      }
    }
    describe ("National ID") {
      it("GET a nationalId by ID") {
        Get("/nationalid/1") ~> AppServer.NationalIdRoute ~> check {
          status shouldEqual StatusCodes.OK
          responseAs[String] shouldEqual TestData.national_id_1.toJson.toString
        }
      }
      it("PUT a new nationalid") {
        Put("/nationalid") ~> AppServer.NationalIdRoute ~> check {
          status shouldEqual StatusCodes.OK
          responseAs[String] shouldEqual "{\"would_be_id\":3}"
        }
      }
      it("list nationalids") {
        Get("/nationalid/list") ~> AppServer.NationalIdRoute ~> check {
          status shouldEqual StatusCodes.OK
          responseAs[String] shouldEqual "{\"ids\": [1,2]}"
        }
      }
    }
    describe ("Address") {
      it("GET an address by ID") {
        Get("/address/1") ~> AppServer.AddressRoute ~> check {
          status shouldEqual StatusCodes.OK
          responseAs[String] shouldEqual TestData.test_address_1.toJson.toString
        }
      }
      it("PUT a new address") {
        Put("/address") ~> AppServer.AddressRoute ~> check {
          status shouldEqual StatusCodes.OK
          responseAs[String] shouldEqual "{\"would_be_id\":3}"
        }
      }
      it("list addresses") {
        Get("/address/list") ~> AppServer.AddressRoute ~> check {
          status shouldEqual StatusCodes.OK
          responseAs[String] shouldEqual "{\"ids\": [1,2]}"
        }
      }
    }

  }
}
