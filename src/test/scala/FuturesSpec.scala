import org.scalatest.{AsyncFunSpec, Matchers}
import scala.concurrent.{Future, Promise}


class FuturesSpec extends AsyncFunSpec with Matchers{

  describe("Future Test") {
    it("should return a valid (future[person], future[address]) tuple") {
      val (future_person, future_address) = AsyncTestData.getTupleFuturePersonFutureAddress
      future_person map { person =>
        person shouldBe (TestData.test_person)
      }
      future_address map { address =>
        address shouldBe (TestData.test_address_1)
      }
    }

    it("should return a valid (future[person], future[address]) tuple in a for comprehension") {
      val (future_person, future_address) = AsyncTestData.getTupleFuturePersonFutureAddress
      val simpleFuture = for{
        person <- future_person map { p => p }
        address <- future_address map { a => a }
      } yield (person, address)

      simpleFuture map { _ match {
        case (p: Person, a: Address) => {
          p shouldBe (TestData.test_person)
          a shouldBe (TestData.test_address_1)
        }
        case (_) => throw new Exception ( "Bad case")
      }}
    }
    describe("Folds"){
      it ("should fold integers"){
        val sum:Int = TestData.listOfInts.foldLeft(0)((a,b) => if (a == 0) b else a - b)
        sum shouldEqual (-43)

        val diff:Int = TestData.listOfInts.foldRight(0)((a,b) => if (a == 0) b else a - b)
        sum shouldEqual (-43)

      }
      it ("should fold doubles"){
        val sum:Double = TestData.listOfDoubles.foldRight(1.0)((a,b) => a * b )
        sum shouldEqual(790020.0)
      }
    }
  }



  /**
    * This method illustrates how to use a promise.  Every Promise is tied to one and only one Future object. SO
    * 1.  Create a promise (p)
    * 2.  Create a future as a containter for async computation
    * 3.  Inside the future, set the promise's 'successful' method to whatever is the result of the async computation
    * 4.  Return the promise's future immediately
    * @param a Integer representing value to add
    * @param b Integer representing value to add
    * @return Future[Int] representing result of computation
    */
  def completeFutureAddtion(a:Int, b:Int): Future[Int] = {
    //create a promise
    val p = Promise[Int]()

    //a simple method to simulate a randomly-long computation
    def getSomethingSlowly(a:Int):Future[Int]= {
      Thread.sleep(100 * a)
      Future.successful(a+1)
    }

    //another one
    def getSomethingElseSlowly(b:Int):Future[Int] = {
      Thread.sleep(110 * b)
      Future.successful(b+2)
    }

    //create a future that, when complete, updates the status of the promise with the result of the computation
    Future {
      val a_slowly = getSomethingSlowly(a)
      val b_slowly = getSomethingElseSlowly(b)
      for {
        a <- a_slowly
        b <- b_slowly
      } yield {
        p.success(a + b)
      }
    }

    //return the promise's companion future now (before the promise resolves)
    p.future
  }

  describe ("Promise") {
    it ("should complete a future when its done"){
      val result:Future[Int] = this.completeFutureAddtion(20, 30)
      result map {r =>
        r shouldEqual(53)
      }
    }

    it("should throw exception if set twice"){
      val p = Promise[Int]()
      p.success(3)
      assertThrows[IllegalStateException]{
        p.success(4)
      }
    }

    it("contains a not-completed Future"){
      val p = Promise[Int]()
      p.future should not be null
      p.future shouldBe a [Future[_]]
      p.future.isCompleted shouldEqual(false)
    }

  }
}
