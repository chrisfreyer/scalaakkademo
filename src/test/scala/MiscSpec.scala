import org.scalatest.{AsyncFunSpec, Matchers}

import scala.concurrent.Await
import scala.concurrent.Await._
import scala.concurrent.duration._


/**
  * The purpose of this class is to illustrate the basic operations that can be done
  * to/with Lists.  Some of these are straightforward, others not so much.
  */
class MiscSpec extends AsyncFunSpec with Matchers {
  describe("Misc Tests") {
    it("for comprehension with filter should return expected output") {
      val expectedOutput = Seq("b", "d")
      val data = Seq(
        1 -> "a",
        2 -> "b",
        3 -> "c",
        4 -> "d"
      )

      val output = for (
        (key: Int, value: String) <- data if key % 2 == 0
      ) yield value
      output shouldEqual expectedOutput
    }


    it("for comprehension should extract & transform input into expected output") {
      val input: List[(Int, List[Int])] = List((2, List(3, 4)), (1, List(2, 3, 4)))
      val expectedOutput = List((2, 3), (2, 4), (1, 2), (1, 3), (1, 4))

      val output = for (
        element <- input;
        y       <- element._2
      ) yield (element._1, y)

      output shouldEqual expectedOutput
    }


    it("collect should gather only the desired data into a list") {
      val input: List[(String, List[String])] = List(
        ("Rock",      List("Journey", "Heart")),
        ("Country",   List("Garth Brooks", "Alan Jackson")),
        ("Classical", List("Bach", "Bethoven", "Wagner")),
        ("Rock",      List("Kiss", "Metallica"))
      )
      val expectedOutput = List("Journey", "Heart", "Kiss", "Metallica")

      val output = input.collect{
        case (genre, artists) if genre == "Rock" => artists
      }

      output.flatten shouldEqual expectedOutput
    }

//    it ("for comprehension should work normally with future") {
//      val input = AsyncTestData.getFuturePerson(1)
//      val expectedOutput = List("Joe Smith", "Marie Jones")
//      val output =  for {
//        person <- input
//      } yield person
//
//      val x = Await.result(output, 20.second)
//      x shouldEqual expectedOutput
//    }
  }
}
