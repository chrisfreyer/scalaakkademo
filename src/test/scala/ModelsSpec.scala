import org.scalatest.{FunSpec, Matchers}
import spray.json._
import DefaultJsonProtocol._

class ModelsSpec extends FunSpec with Matchers{

  describe ("Model") {
    describe ("Person") {
      it("finds national ID object inside person") {
        TestData.test_person.nationalId should not be(None)
      }
      it("finds the right national ID object inside person") {
        TestData.test_person.nationalId shouldBe  (Some(NationalID(nationalIdType = "SSN", nationalIdValue = "1234567890")))
      }
      it("finds address object inside person") {
        assert(TestData.test_person.address.size == 1)
      }
    }
  }
  describe ("JSON serializaers"){
    val nationalIdString = """{"nationalIdType":"SSN","nationalIdValue":"1234567890"}"""
    val addressString    = """{"city":"City1","state":"State1","country":"USA","postalCode":"Zip1","addressType":"Home","street":"Street1"}"""
    val personString = """{"nationalId":"""+nationalIdString+""","age":33,"lastName":"Smith","firstName":"Joe","id":1,"address":["""+addressString+"""]}"""
    val personIdsString = """{"ids":[1,2,3]}"""

      it ("should marshal/unmarshall a NationalID"){
        val idString = TestData.national_id_1.toJson.toString()
        idString shouldEqual(nationalIdString)
        val obj:NationalID = idString.parseJson.convertTo[NationalID]
        obj shouldEqual(TestData.national_id_1)
      }
    it ("should marshal/unmarshall an Address"){
      val address = TestData.test_address_1.toJson.toString()
      address shouldEqual(addressString)
      val obj:Address = address.parseJson.convertTo[Address]
      obj shouldEqual(TestData.test_address_1)
    }
    it ("should marshal/unmarshall a Person"){
      val person = TestData.test_person.toJson.toString()
      person shouldEqual(personString)
      val obj:Person = person.parseJson.convertTo[Person]
      obj shouldEqual(TestData.test_person)
    }
    it ("should marshal/unmarshall PersonIds"){
      val personIds = TestData.personIds.toJson.toString()
      personIds shouldEqual(personIdsString)
      val obj:PersonIds = personIds.parseJson.convertTo[PersonIds]
      obj shouldEqual(TestData.personIds)
    }

  }
}


