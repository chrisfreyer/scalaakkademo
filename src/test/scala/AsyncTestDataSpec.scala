import org.scalatest.{AsyncFunSpec, Matchers}
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

class AsyncTestDataSpec extends AsyncFunSpec with Matchers{
  describe ("Factory"){

    describe ("getFuturePerson"){
      it("will eventually return a person"){
        val person:Future[Person] = AsyncTestData.getFuturePerson(1)
        person map { p =>
          p shouldEqual (TestData.test_person)
        }
      }
    }

    describe ("getFutureAddress"){
      it ("returns object of the expected shape"){
        val future_address: Future[Address] = AsyncTestData.getFutureAddress(1)
        val address = Await.result(future_address, 1 second)
        address shouldEqual TestData.test_address_1
      }
    }

    describe ("getTupleFuturePersonFutureAddress"){
      it ("will eventaully return the expected shape"){
        val tupleOfFutures = AsyncTestData.getTupleFuturePersonFutureAddress
        tupleOfFutures._1 map { person =>
          person shouldEqual(TestData.test_person)
        }
        tupleOfFutures._2 map { address =>
          address shouldEqual(TestData.test_address_1)
        }
      }
    }

    describe ("getFutureListOfPersons"){
      it ("returns object of the expected shape"){
        val future_persons: Future[Seq[Person]] = AsyncTestData.getFutureListOfPersons
        val persons = Await.result(future_persons, 1 second)
        persons shouldEqual Seq(TestData.test_person, TestData.test_person_2)
      }
    }

    describe ("getListOfFuturePersons"){
      it ("returns object of the expected shape"){
        val listOfFuturePersons: Seq[Future[Person]] = AsyncTestData.getListOfFuturePersons
        //convert seq[future[address]] to future[seq[address]]
        val myFutureList: Future[Seq[Person]] = Future.sequence(listOfFuturePersons)
        myFutureList map { myList =>
          myList shouldEqual Seq(TestData.test_person, TestData.test_person_2)
        }
      }
    }

    describe ("getFutureListOfAddresses"){
      it ("returns object of the expected shape"){
        val future_addresses: Future[Seq[Address]] = AsyncTestData.getFutureListOfAddresses
        val addresses = Await.result(future_addresses, 1 second)
        addresses shouldEqual Seq(TestData.test_address_1, TestData.test_address_2)
      }
    }

    describe ("getListOfFutureAddresses"){
      it ("returns object of the expected shape"){
        val listOfFutureAddresses: Seq[Future[Address]] = AsyncTestData.getListOfFutureAddresses
        //convert seq[future[address]] to future[seq[address]]
        val myFutureList: Future[Seq[Address]] = Future.sequence(listOfFutureAddresses)
        myFutureList map { myList =>
          myList shouldEqual Seq(TestData.test_address_1, TestData.test_address_2)
        }
      }
    }
  }
}
