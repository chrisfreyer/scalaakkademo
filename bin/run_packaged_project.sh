#! /usr/local/bin/bash
# Script assumes 'sbt package'  has been run to jar the project and download dependency jars in lib_managed folder

PROJECT_JAR=/Users/chris.freyer/IdeaProjects/ScalaAkkaDemo/target/scala-2.12/testproject_2.12-0.1.jar

# detect if project jar is missing
if [ ! -e $PROJECT_JAR ]; then
    echo "Project jar not found.  Quitting..."
    exit
fi

# create a classpath from jars pulled from sbt
shopt -s globstar
THE_CLASSPATH=
for i in /Users/chris.freyer/IdeaProjects/ScalaAkkaDemo/lib_managed/**/*.jar; do
  THE_CLASSPATH=${THE_CLASSPATH}:${i}
done

# prepend the project jar file to the classpath
CLASSPATH=$PROJECT_JAR:$THE_CLASSPATH

# run the app server
/usr/bin/java -cp $CLASSPATH AppServer

