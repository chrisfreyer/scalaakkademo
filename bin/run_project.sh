#! /usr/local/bin/bash
# Script assumes 'sbt compile' has been run to create classes and download dependency jars in lib_managed folder

PROJECT_CLASS_DIR=/Users/chris.freyer/IdeaProjects/ScalaAkkaDemo/target/scala-2.12/classes
PROJECT_RESOURCES_DIR=/Users/chris.freyer/IdeaProjects/ScalaAkkaDemo/src/main/resources

# detect if project class dir is missing
if [ ! -e $PROJECT_CLASS_DIR ]; then
    echo "Project class directory not found.  Quitting..."
    exit
fi

# detect if no classes are in the class dir
cnt=$( find $PROJECT_CLASS_DIR -name "*.class" | wc -l )
if [ ! $cnt -gt 0 ]; then
    echo "No classes found in project class directory.  Quitting..."
    exit
fi

# create a classpath from jars pulled from sbt
shopt -s globstar
THE_CLASSPATH=
for i in /Users/chris.freyer/IdeaProjects/ScalaAkkaDemo/lib_managed/**/*.jar; do
  THE_CLASSPATH=${THE_CLASSPATH}:${i}
done

# prepend the project jar file to the classpath
CLASSPATH=$PROJECT_CLASS_DIR:$PROJECT_RESOURCES_DIR:$THE_CLASSPATH

# run the app server
/usr/bin/java -cp $CLASSPATH AppServer

