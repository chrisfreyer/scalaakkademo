import java.io.File

import sbt.Tests.Setup

name := "ScalaAkkaDemo"

version := "0.1"

scalaVersion := "2.12.3"

libraryDependencies ++= Seq(
  "org.scalactic"     %% "scalactic"            % "3.0.1",
  "org.scalatest"     %% "scalatest"            % "3.0.1"     % "test",
  "com.typesafe.akka" %% "akka-http-spray-json" % "10.0.10",
  "com.typesafe.akka" %% "akka-http"            % "10.0.10",
  "com.typesafe.akka" %% "akka-http-testkit"    % "10.0.10"   % Test,
  "com.typesafe.akka" %% "akka-testkit"         % "2.5.4"     % Test,
  "com.typesafe.akka" %% "akka-slf4j"           % "2.5.4",
  "com.typesafe.akka" %% "akka-cluster"         % "2.5.4",
  "com.typesafe.akka" %% "akka-actor"           % "2.5.4",
  "ch.qos.logback"    % "logback-classic"       % "1.2.3"
)



dependencyOverrides ++= Set(
)

resolvers += "spray repo" at "http://repo.spray.io"

logBuffered in Test := false
retrieveManaged := true

logLevel in Test := Level.Info

//force sbt to load slf4j before anything else...
testOptions += Setup( cl =>
  cl.loadClass("org.slf4j.LoggerFactory").
    getMethod("getLogger",cl.loadClass("java.lang.String")).
    invoke(null,"ROOT")
)