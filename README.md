
This is a demo project to illustrate basic scala/akka conecepts and patterns.  The app is a basic restful service.  Some of the tests in src/test/scala are for the service, but other are to illustrate different concepts.  

You're welcome to use this code, but:

* always assume it's a work in progress
* run 'sbt test' before use
* don't copy/paste unless you know what you're doing
* your money back if you're not completely delighted

Complete:

1. implement a scala/spray RESTful service
2. add reusable test data (todo: refactor this as a trait?)
3. add typesafe config
4. document Future transformations (in tests)
5. Demonstrate Promise + Future together in a test

Todo:

1. Crate a trait to bring in execution context
2. Ditto for logging (maybe combine them?)
4. Experiment with FlatSpec testing style
5. Demonstrate FeatureSpec with GivenWhenThen in a test

Author:  [~chris.freyer]

